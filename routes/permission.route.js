const express = require("express");
const router = express.Router();
const permissionService = require("../services/permission.service");

// middleware that is specific to this router
router.use((req, res, next) => {
  console.log("Time: ", Date.now());
  next();
});
// define the home page route
router.get("/", (req, res) => {
  res.send("permissions API 1.0");
});

router.get("/get-all-permission", async (req, res) => {
  let result = await permissionService.getAllpermission();
  res.status(200).send({
    status: 200,
    success: true,
    data: result,
    message: "permission fetched successfully",
  });
});

//update permission
router.put("/update/:id", (req, res) => {
  console.log("Routes", req.params.id);
  try {
    const result = permissionService.updatepermission(req.params.id, req.body);
    res.send({
      status: 204,
      data: req.params.id,
      success: true,
      message: "Updated succefully",
    });
  }
  catch (err) {
    res.send({
      status: 500,
      data: undefined,
      success: true,
      message: err,
    });
  };
});
//find by id
router.get("/find/:id", async (req, res) => {
  console.log("Routes", req.params.id);
  try {
    const result = await permissionService.findByid(req.params.id);
    if (!result) {
      // If no document was found with the specified ID, send a 404 status code
      res.status(404).send({
        success: false,
        message: "Document not found",
      });
    } else {
      res.send({
        status: 200,
        data: result,
        success: true,
        message: "Document retrieved successfully",
      });
    }
  } catch (err) {
    res.status(500).send({
      success: false,
      message: err.message,
    });
  }
});

//find by name
router.get("/find", (req, res) => {
  permissionService.findByName(req.query.name)
    .then((data) => {
      res.status(200).send({
        status: 200,
        success: true,
        data: data,
        message: "permission fetched successfully",
      });
    })
    .catch((err) => {
      res.status(500).send({
        status: 500,
        success: false,
        data: null,
        message: "Internal server error",
      });
    });
});

//post permission
router.post("/add-permission-data", (req, res) => {

  permissionService.save(req.body)
    .then(function (data) {
      res.status(200).send({
        status: 200,
        success: true,
        data: data,
        message: "fetched permission successfully",
      });
    })
    .catch((err) => {
      res.status(500).send({
        status: 500,
        success: false,
        data: null,
        message: "Internal server error",
      });
    });
});


//delete permission
router.delete("/delete/:id", async (req, res) => {
  const result = await permissionService.deletepermission(req.params.id);
  console.log(result);
  res.send({ 
    messsage: 'data deleted',
     data: result,
      success: true 
    });
});



module.exports = router;
