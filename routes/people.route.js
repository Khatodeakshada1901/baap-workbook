const express = require("express");
const router = express.Router();
const peopleService = require("../services/people.service");

// middleware that is specific to this router
router.use((req, res, next) => {
  console.log("Time: ", Date.now());
  next();
});
// define the home page route
router.get("/", (req, res) => {
  res.send("peoples API 1.0");
});

router.get("/get-all-people", async (req, res) => {
  let result = await peopleService.getAllpeople();
  res.status(200).send({
    status: 200,
    success: true,
    data: result,
    message: "people fetched successfully",
  });
});


//find by id
router.get("/find/:id", async (req, res) => {
  console.log("Routes", req.params.id);
  try {
    const result = await peopleService.findByid(req.params.id);
    if (!result) {
      // If no document was found with the specified ID, send a 404 status code
      res.status(404).send({
        success: false,
        message: "Document not found",
      });
    } else {
      res.send({
        status: 200,
        data: result,
        success: true,
        message: "Document retrieved successfully",
      });
    }
  } catch (err) {
    res.status(500).send({
      success: false,
      message: err.message,
    });
  }
});
//update people
router.put("/update/:id", (req, res) => {
  console.log("Routes", req.params.id);
  try {
    const result = peopleService.updatepeople(req.params.id, req.body);
    res.send({
      status: 204,
      data: req.params.id,
      success: true,
      message: "Updated succefully",
    });
  }
  catch (err) {
    res.send({
      status: 500,
      data: undefined,
      success: true,
      message: err,
    });
  };
});


//find by name
router.get("/find", (req, res) => {
  peopleService.findByName(req.query.name)
    .then((data) => {
      res.status(200).send({
        status: 200,
        success: true,
        data: data,
        message: "people fetched successfully",
      });
    })
    .catch((err) => {
      res.status(500).send({
        status: 500,
        success: false,
        data: null,
        message: "Internal server error",
      });
    });
});

//post people
router.post("/add-people-data", (req, res) => {

  peopleService.save(req.body)
    .then(function (data) {
      res.status(200).send({
        status: 200,
        success: true,
        data: data,
        message: "fetched people successfully",
      });
    })
    .catch((err) => {
      res.status(500).send({
        status: 500,
        success: false,
        data: null,
        message: "Internal server error",
      });
    });
});


//delete people
router.delete("/delete/:id", async (req, res) => {
  const result = await peopleService.deletepeople(req.params.id);
  console.log(result);
  res.send({ 
    messsage: 'data deleted',
     data: result,
      success: true 
    });
});



module.exports = router;
