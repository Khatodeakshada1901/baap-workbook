const express = require("express");
const router = express.Router();
const groupappService = require("../services/groupapp.service");

// middleware that is specific to this router
router.use((req, res, next) => {
  console.log("Time: ", Date.now());
  next();
});
// define the home page route
router.get("/", (req, res) => {
  res.send("groupapps API 1.0");
});

router.get("/get-all-groupapp", async (req, res) => {
  let result = await groupappService.getAllgroupapp();
  res.status(200).send({
    status: 200,
    success: true,
    data: result,
    message: "groupapp fetched successfully",
  });
});

//id

router.get("/find/:id", async (req, res) => {
  console.log("Routes", req.params.id);
  try {
    const result = await groupappService.findByid(req.params.id);
    if (!result) {
      // If no document was found with the specified ID, send a 404 status code
      res.status(404).send({
        success: false,
        message: "Document not found",
      });
    } else {
      res.send({
        status: 200,
        data: result,
        success: true,
        message: "Document retrieved successfully",
      });
    }
  } catch (err) {
    res.status(500).send({
      success: false,
      message: err.message,
    });
  }
});
//update groupapp
router.put("/update/:id", (req, res) => {
  console.log("Routes", req.params.id);
  try {
    const result = groupappService.updategroupapp(req.params.id, req.body);
    res.send({
      status: 204,
      data: req.params.id,
      success: true,
      message: "Updated succefully",
    });
  }
  catch (err) {
    res.send({
      status: 500,
      data: undefined,
      success: true,
      message: err,
    });
  };
});


//find by name
router.get("/find", (req, res) => {
  groupappService.findByName(req.query.name)
    .then((data) => {
      res.status(200).send({
        status: 200,
        success: true,
        data: data,
        message: "groupapp fetched successfully",
      });
    })
    .catch((err) => {
      res.status(500).send({
        status: 500,
        success: false,
        data: null,
        message: "Internal server error",
      });
    });
});

//post groupapp
router.post("/add-groupapp-data", (req, res) => {

  groupappService.save(req.body)
    .then(function (data) {
      res.status(200).send({
        status: 200,
        success: true,
        data: data,
        message: "fetched groupapp successfully",
      });
    })
    .catch((err) => {
      res.status(500).send({
        status: 500,
        success: false,
        data: null,
        message: "Internal server error",
      });
    });
});


//delete groupapp
router.delete("/delete/:id", async (req, res) => {
  const result = await groupappService.deletegroupapp(req.params.id);
  console.log(result);
  res.send({ 
    messsage: 'data deleted',
     data: result,
      success: true 
    });
});



module.exports = router;
