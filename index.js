const express = require("express");
const cors = require('cors');
// const coursesRoutes = require("./routes/courses.routes");

const db = require("./config/db");
const app = express();
app.use(cors())

const bodyParser = require("body-parser");
const appRoute = require("./routes/app.route")
const course=require("./routes/course.routes")
const customer=require("./routes/customer.route")
const Admin=require("./routes/Admin.route")
const user=require("./routes/user.route");
const lession=require("./routes/lession.route");
const coupons=require("./routes/coupons.route");
const cart=require("./routes/cart.route");
const bag=require("./routes/bag.route");
const wishlist=require("./routes/wishlist.route");
const usergroup=require("./routes/usergroup.route");
const group=require("./routes/group.route");
const role=require("./routes/role.route");
const assignments=require("./routes/assignments.route");
const students=require("./routes/students.route");
const manager=require("./routes/manager.route");
const groupapp=require("./routes/groupapp.route");
const userapp=require("./routes/userapp.route");
const brand=require("./routes/brand.route");
const product=require("./routes/product.route");
const family=require("./routes/family.route");
const hostel=require("./routes/hostel.route");
const fee=require("./routes/fee.route");
const feesdata=require("./routes/feesdata.route");
const login=require("./routes/login.route");
const registration=require("./routes/registration.route");
const supplier = require("./routes/supplier.route");
const request = require("./routes/request.route");
const offer = require("./routes/offer.route");
const order = require("./routes/order.route");
const categories = require("./routes/categorise.route");
const subcategories = require("./routes/subcategories.route");
const booking = require("./routes/booking.route");
const store = require("./routes/store.route");
const shipping = require("./routes/shipping.route");
const list = require("./routes/list.route");
const canteen = require("./routes/canteen.route");
const leave = require("./routes/leave.route");
const post = require("./routes/post.route");
const attendance = require("./routes/attendance.route");
const vehical = require("./routes/vehical.route");
const people = require("./routes/people.route");
const permission = require("./routes/permission.route");
const agent = require("./routes/agent.route");
const addres = require("./routes/addres.route");
const useraddres = require("./routes/useraddres.route");
const SuperAdmin = require("./routes/SuperAdmin.route");
const technician = require("./routes/technician.route");
const personal = require("./routes/personal.route");
// const chaie=require("./routes/chair.route");
const apollo = require("./routes/apollo.route");
const astron = require("./routes/astron.route");
const pheonix = require("./routes/pheonix.route");
const severe = require("./routes/severe.route");
const random = require("./routes/random.route");
const bongo = require("./routes/bongo.route");
const test = require("./routes/test.route");
const formula = require("./routes/formula.route");
const define = require("./routes/define.route");
const next = require("./routes/next.route");
const chair = require("./routes/chair.route");


const result = require("./routes/result.route");
const folder = require("./routes/folder.route");
const code = require("./routes/code.route");
const create = require("./routes/create.route");
const modul = require("./routes/modul.route");
const developer = require("./routes/developer.route");
const familyinfo = require("./routes/familyinfo.route");






// support parsing of application/json type post data
app.use(bodyParser.json());

//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send("Hello everyone!");
});
app.use("/app",appRoute)
app.use("/course",course)
app.use("/customer",customer)
app.use("/Admin",Admin)
app.use("/user",user)
app.use("/lession",lession)
app.use("/coupons",coupons)
app.use("/cart",cart)
app.use("/bag",bag)
app.use("/wishlist",wishlist)
app.use("/usergroup",usergroup)
app.use("/group",group)
app.use("/role",role)
app.use("/assignments",assignments)
app.use("/students",students)
app.use("/manager",manager)
app.use("/groupapp",groupapp)
app.use("/userapp",userapp)
app.use("/brand",brand)
app.use("/product",product)
app.use("/family",family)
app.use("/hostel",hostel)
app.use("/fee",fee)
app.use("/feesdata",feesdata)
app.use("/login",login)
app.use("/registration",registration)
app.use("/supplier",supplier)
app.use("/request",request)
app.use("/offer",offer)
app.use("/order",order)
app.use("/categories",categories)
app.use("/subcategories",subcategories)
app.use("/booking",booking)
app.use("/store",store)
app.use("/shipping",shipping)
app.use("/list",list)
app.use("/attendance",attendance)
app.use("/post",post)
app.use("/vehical",vehical)
app.use("/permission",permission)
app.use("/agent",agent)
app.use("/permission",permission)
app.use("/addres",addres)
app.use("/useraddres",useraddres)
app.use("/SuperAdmin",SuperAdmin)
app.use("/technician",technician)
app.use("/personal",personal)
app.use("/apollo",apollo)
app.use("/astron",astron)

app.use("/pheonix",pheonix)
app.use("/severe",severe)
app.use("/random",random)

app.use("/bongo",bongo)
app.use("/test",test)
app.use("/formula",formula)
app.use("/define",define)
app.use("/next",next)
app.use("/chair",chair)


app.use("/create",create)
app.use("/modul",modul)
app.use("/result",result)
app.use("/developer",developer)
app.use("/create",create)
app.use("/familyinfo",familyinfo)
// app.use("/course", coursesRoutes);

// app.listen(port, () => {
//   console.log(`Example app listening on port ${port}`)
// })

module.exports = app;
