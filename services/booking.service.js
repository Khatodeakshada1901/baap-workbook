const booking = require("../models/booking.model");
const booking1 = {


  getAllbooking: async function () {
    return await booking.find();
  },

  save: async function (bookingObject) {
    return await booking.create(bookingObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await booking.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete booking
  deletebooking: function(id) {
    return new Promise((resolve, reject) => {
      booking.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      booking.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  updatebooking: async function(id, topic) {
    booking.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = booking1;

