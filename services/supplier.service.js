const supplier= require("../models/supplier.model");
const supplier1 = {


  getAllsupplier: async function () {
    return await supplier.find();
  },

  save: async function (groupObject) {
    return await supplier.create(groupObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     supplier.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await  supplier.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // deletesupplier
  deletesupplier: function(id) {
    return new Promise((resolve, reject) => {
     supplier.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatesupplier: async function(id, topic) {
   supplier.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports =supplier1;

