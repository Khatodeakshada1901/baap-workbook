const test = require("../models/test.model");
const test1 = {


  getAlltest: async function () {
    return await test.find();
  },

  save: async function (testObject) {
    return await test.create(testObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await test.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  gettestByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const test = await testModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: test,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletetest: function(id) {
    return new Promise((resolve, reject) => {
      test.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      test.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatetest: async function(id, topic) {
    test.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = test1;