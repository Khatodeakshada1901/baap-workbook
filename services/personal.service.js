const personal = require("../models/personal.model");
const personal1 = {


  getAllpersonal: async function () {
    return await personal.find();
  },

  save: async function (personalObject) {
    return await personal.create(personalObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     personal.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await personal.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete personal
  deletepersonal: function(id) {
    return new Promise((resolve, reject) => {
      personal.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatepersonal: async function(id, topic) {
    personal.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = personal1;

