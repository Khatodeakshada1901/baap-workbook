const code = require("../models/code.model");
const code1 = {


  getAllcode: async function () {
    return await code.find();
  },

  save: async function (codeObject) {
    return await code.create(codeObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await code.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getcodeByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const code = await codeModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: code,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletecode: function(id) {
    return new Promise((resolve, reject) => {
      code.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      code.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatecode: async function(id, topic) {
    code.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = code1;

