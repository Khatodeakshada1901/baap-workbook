const technician = require("../models/technician.model");
const technician1 = {


  getAlltechnician: async function () {
    return await technician.find();
  },

  save: async function (technicianObject) {
    return await technician.create(technicianObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await technician.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     technician.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },
  // delete technician
  deletetechnician: function(id) {
    return new Promise((resolve, reject) => {
      technician.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatetechnician: async function(id, topic) {
    technician.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = technician1;

