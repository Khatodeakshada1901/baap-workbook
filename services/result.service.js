const result = require("../models/result.model");
const result1 = {


  getAllresult: async function () {
    return await result.find();
  },

  save: async function (resultObject) {
    return await result.create(resultObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await result.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getresultByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await resultModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: result,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deleteresult: function(id) {
    return new Promise((resolve, reject) => {
      result.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      result.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateresult: async function(id, topic) {
    result.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = result1;

