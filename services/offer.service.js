const offer = require("../models/offer.model");
const offer1 = {


  getAlloffer: async function () {
    return await offer.find();
  },

  save: async function (offerObject) {
    return await offer.create(offerObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     offer.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await offer.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete offer
  deleteoffer: function(id) {
    return new Promise((resolve, reject) => {
      offer.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateoffer: async function(id, topic) {
    offer.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = offer1;

