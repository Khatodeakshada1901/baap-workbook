const registration = require("../models/registration.model");
const registration1 = {


  getAllregistration: async function () {
    return await registration.find();
  },

  save: async function (registrationObject) {
    return await registration.create(registrationObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     registration.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await registration.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete registration
  deleteregistration: function(id) {
    return new Promise((resolve, reject) => {
      registration.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateregistration: async function(id, topic) {
    registration.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = registration1;

