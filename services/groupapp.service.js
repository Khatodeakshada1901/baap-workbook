const groupapp = require("../models/groupapp.model");
const groupapps = {


  getAllgroupapp: async function () {
    return await groupapp.find();
  },

  save: async function (groupappObject) {
    return await groupapp.create(groupappObject);
  },

  //find by id
  findByid: function(id)
  {
     return new Promise((resolve, reject) => {
       groupapp.findById(id, function(err, doc) {
         if (err) {
           console.log(err);
           reject(err);
         } else {
           resolve(doc);
         }
       });
     });
   },

  //find by name
  findByName: async function (name) {
    const data = await groupapp.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete groupapp
  deletegroupapp: function(id) {
    return new Promise((resolve, reject) => {
      groupapp.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updategroupapp: async function(id, topic) {
    groupapp.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = groupapps;

