const group = require("../models/group.model");
const group1 = {


  getAllgroup: async function () {
    return await group.find();
  },

  save: async function (groupObject) {
    return await group.create(groupObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await group.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  //find by id
  findByid: function(id)
  {
     return new Promise((resolve, reject) => {
       group.findById(id, function(err, doc) {
         if (err) {
           console.log(err);
           reject(err);
         } else {
           resolve(doc);
         }
       });
     });
   },
   
  // delete group
  deletegroup: function(id) {
    return new Promise((resolve, reject) => {
      group.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updategroup: async function(id, topic) {
    group.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = group1;

