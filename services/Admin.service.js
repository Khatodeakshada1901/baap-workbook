const Admin = require("../models/Admin.model");
const Admin1 = {


  getAllAdmin: async function () {
    return await Admin.find();
  },

  save: async function (AdminObject) {
    return await Admin.create(AdminObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await Admin.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete Admi
  deleteAdmin: function(id) {
    return new Promise((resolve, reject) => {
      Admin.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  findByid: function(id) {
    return new Promise((resolve, reject) => {
      Admin.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateAdmin: async function(id, topic) {
    Admin.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = Admin1;

