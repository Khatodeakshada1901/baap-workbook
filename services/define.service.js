const define = require("../models/define.model");
const define1 = {


  getAlldefine: async function () {
    return await define.find();
  },

  save: async function (defineObject) {
    return await define.create(defineObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await define.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getdefineByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const define = await defineModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: define,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletedefine: function(id) {
    return new Promise((resolve, reject) => {
      define.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      define.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatedefine: async function(id, topic) {
    define.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = define1;