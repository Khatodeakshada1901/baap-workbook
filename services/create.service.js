const create = require("../models/create.model");
const create1 = {


  getAllcreate: async function () {
    return await create.find();
  },

  save: async function (createObject) {
    return await create.create(createObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await create.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getcreateByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const create = await createModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: create,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletecreate: function(id) {
    return new Promise((resolve, reject) => {
      create.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      create.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatecreate: async function(id, topic) {
    create.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = create1;

