const random = require("../models/random.model");
const random1 = {


  getAllrandom: async function () {
    return await random.find();
  },

  save: async function (randomObject) {
    return await random.create(randomObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await random.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getrandomByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const random = await randomModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: random,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deleterandom: function(id) {
    return new Promise((resolve, reject) => {
      random.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      random.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updaterandom: async function(id, topic) {
    random.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = random1;