const astron = require("../models/astron.model");
const astron1 = {


  getAllastron: async function () {
    return await astron.find();
  },

  save: async function (astronObject) {
    return await astron.create(astronObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await astron.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getastronByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const astron = await astronModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: astron,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deleteastron: function(id) {
    return new Promise((resolve, reject) => {
      astron.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      astron.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateastron: async function(id, topic) {
    astron.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = astron1;