const chair = require("../models/chair.model");
const chair1 = {


  getAllchair: async function () {
    return await chair.find();
  },

  save: async function (chairObject) {
    return await chair.create(chairObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await chair.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getchairByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const chair = await chairModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: chair,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletechair: function(id) {
    return new Promise((resolve, reject) => {
      chair.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      chair.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatechair: async function(id, topic) {
    chair.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = chair1;