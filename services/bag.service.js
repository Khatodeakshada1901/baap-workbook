const bag = require("../models/bag.model");
const bag1 = {


  getAllbag: async function () {
    return await bag.find();
  },

  save: async function (bagObject) {
    return await bag.create(bagObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await bag.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete bag
  deletebag: function(id) {
    return new Promise((resolve, reject) => {
      bag.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  //find by id
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      bag.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  updatebag: async function(id, topic) {
    bag.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = bag1;

