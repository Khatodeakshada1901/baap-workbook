const agent = require("../models/agent.model");
const agent1 = {


  getAllagent: async function () {
    return await agent.find();
  },

  save: async function (agentObject) {
    return await agent.create(agentObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await agent.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete Admi
  deleteagent: function(id) {
    return new Promise((resolve, reject) => {
      agent.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  

  //id

    
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      agent.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
//   updateagent: async function(id, topic) {
//     agent.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
//       if(err){
//         console.log("error ", err)
//         return false;
//       } else {
//         console.log("dataa ",doc);
//         return doc
//       }
//     });
//   },
};




module.exports = agent1;

