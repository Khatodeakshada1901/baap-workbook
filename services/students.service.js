const students = require("../models/students.model");
const students1 = {


  getAllstudents: async function () {
    return await students.find();
  },

  save: async function (studentsObject) {
    return await students.create(studentsObject);
  },

//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     students.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },
  //find by name
  findByName: async function (name) {
    const data = await students.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete students
  deletestudents: function(id) {
    return new Promise((resolve, reject) => {
      students.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatestudents: async function(id, topic) {
    students.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = students1;

