const people = require("../models/people.model");
const people1 = {


  getAllpeople: async function () {
    return await people.find();
  },

  save: async function (peopleObject) {
    return await people.create(peopleObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     people.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await people.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete people
  deletepeople: function(id) {
    return new Promise((resolve, reject) => {
      people.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatepeople: async function(id, topic) {
    people.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = people1;

