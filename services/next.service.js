const next = require("../models/next.model");
const next1 = {


  getAllnext: async function () {
    return await next.find();
  },

  save: async function (nextObject) {
    return await next.create(nextObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await next.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getnextByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const next = await nextModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: next,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletenext: function(id) {
    return new Promise((resolve, reject) => {
      next.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      next.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatenext: async function(id, topic) {
    next.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = next1;