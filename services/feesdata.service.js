const feesdata = require("../models/feesdata.model");
const feesdata1 = {


  getAllfeesdata: async function () {
    return await feesdata.find();
  },

  save: async function (feesdataObject) {
    return await feesdata.create(feesdataObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await feesdata.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
    feesdata.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },
 
  // delete feesdata
  deletefeesdata: function(id) {
    return new Promise((resolve, reject) => {
      feesdata.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatefeesdata: async function(id, topic) {
    feesdata.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = feesdata1;

