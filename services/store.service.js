const store= require("../models/store.model");
const store1 = {


  getAllstore: async function () {
    return await store.find();
  },

  save: async function (storeObject) {
    return await store.create(storeObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     store.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await store.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete store
  deletestore: function(id) {
    return new Promise((resolve, reject) => {
      store.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatestore: async function(id, topic) {
    store.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = store1;

