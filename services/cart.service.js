const cart = require("../models/cart.model");
const cart1 = {


  getAllcart: async function () {
    return await cart.find();
  },

  save: async function (cartObject) {
    return await cart.create(cartObject);
  },

  findByid: function(id) {
    return new Promise((resolve, reject) => {
      cart.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  //find by name
  findByName: async function (name) {
    const data = await cart.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete cart
  deletecart: function(id) {
    return new Promise((resolve, reject) => {
      cart.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatecart: async function(id, topic) {
    cart.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = cart1;

