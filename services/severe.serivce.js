const severe = require("../models/severe.model");
const severe1 = {


  getAllsevere: async function () {
    return await severe.find();
  },

  save: async function (severeObject) {
    return await severe.create(severeObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await severe.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getsevereByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const severe = await severeModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: severe,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletesevere: function(id) {
    return new Promise((resolve, reject) => {
      severe.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      severe.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatesevere: async function(id, topic) {
    severe.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = severe1;

