const bongo = require("../models/bongo,model");
const bongo1 = {


  getAllbongo: async function () {
    return await bongo.find();
  },

  save: async function (bongoObject) {
    return await bongo.create(bongoObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await bongo.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getbongoByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const bongo = await bongoModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: bongo,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletebongo: function(id) {
    return new Promise((resolve, reject) => {
      bongo.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      bongo.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatebongo: async function(id, topic) {
    bongo.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = bongo1;

