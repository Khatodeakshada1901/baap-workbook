const list = require("../models/list.model");
const list1 = {


  getAlllist: async function () {
    return await list.find();
  },

  save: async function (listObject) {
    return await list.create(listObject);
  },


  //find by id
  findByid: function(id)
  {
     return new Promise((resolve, reject) => {
       list.findById(id, function(err, doc) {
         if (err) {
           console.log(err);
           reject(err);
         } else {
           resolve(doc);
         }
       });
     });
   },
  //find by name
  findByName: async function (name) {
    const data = await list.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete list
  deletelist: function(id) {
    return new Promise((resolve, reject) => {
      list.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatelist: async function(id, topic) {
    list.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = list1;

