const Customer = require("../models/customer.model");
const Customer1 = {


  getAllCustomer: async function () {
    return await Customer.find();
  },

  save: async function (CustomerObject) {
    return await Customer.create(CustomerObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await Customer.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },
  findByid: function(id)
  {
     return new Promise((resolve, reject) => {
       Customer.findById(id, function(err, doc) {
         if (err) {
           console.log(err);
           reject(err);
         } else {
           resolve(doc);
         }
       });
     });
   },
   
  // delete Customer
  deleteCustomer: function(id) {
    return new Promise((resolve, reject) => {
      Customer.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateCustomer: async function(id, topic) {
    Customer.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = Customer1;

