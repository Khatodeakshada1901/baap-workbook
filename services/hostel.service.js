const hostel = require("../models/hostel.model");
const hostel1 = {


  getAllhostel: async function () {
    return await hostel.find();
  },

  save: async function (hostelObject) {
    return await hostel.create(hostelObject);
  },

  //find by id
  findByid: function(id)
  {
     return new Promise((resolve, reject) => {
       hostel.findById(id, function(err, doc) {
         if (err) {
           console.log(err);
           reject(err);
         } else {
           resolve(doc);
         }
       });
     });
   },

  //find by name
  findByName: async function (name) {
    const data = await hostel.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete hostel
  deletehostel: function(id) {
    return new Promise((resolve, reject) => {
      hostel.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatehostel: async function(id, topic) {
    hostel.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = hostel1;

