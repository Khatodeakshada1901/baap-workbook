const pheonix = require("../models/pheonix.model");
const pheonix1 = {


  getAllpheonix: async function () {
    return await pheonix.find();
  },

  save: async function (pheonixObject) {
    return await pheonix.create(pheonixObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await pheonix.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getpheonixByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const pheonix = await pheonixModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: pheonix,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletepheonix: function(id) {
    return new Promise((resolve, reject) => {
      pheonix.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      pheonix.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatepheonix: async function(id, topic) {
    pheonix.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = pheonix1;

