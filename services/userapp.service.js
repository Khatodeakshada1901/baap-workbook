const userapp = require("../models/userapp.model");
const userapp1 = {


  getAlluserapp: async function () {
    return await userapp.find();
  },

  save: async function (userappObject) {
    return await userapp.create(userappObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     userapp.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await userapp.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete userapp
  deleteuserapp: function(id) {
    return new Promise((resolve, reject) => {
      userapp.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateuserapp: async function(id, topic) {
    userapp.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = userapp1;

