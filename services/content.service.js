const content = require("../models/content.model");
const content1 = {


  getAllcontent: async function () {
    return await content.find();
  },

  save: async function (contentObject) {
    return await content.create(contentObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await content.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getcontentByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const content = await contentModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: content,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletecontent: function(id) {
    return new Promise((resolve, reject) => {
      content.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      content.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatecontent: async function(id, topic) {
    content.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = content1;

