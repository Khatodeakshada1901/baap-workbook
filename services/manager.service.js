const manager = require("../models/manager.model");
const managers = {


  getAllmanager: async function () {
    return await manager.find();
  },

  save: async function (managerObject) {
    return await manager.create(managerObject);
  },
//find by id
  findByid: function(id)
  {
     return new Promise((resolve, reject) => {
       manager.findById(id, function(err, doc) {
         if (err) {
           console.log(err);
           reject(err);
         } else {
           resolve(doc);
         }
       });
     });
   },
   
  //find by name
  findByName: async function (name) {
    const data = await manager.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete manager
  deletemanager: function(id) {
    return new Promise((resolve, reject) => {
      manager.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatemanager: async function(id, topic) {
    manager.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = managers;

