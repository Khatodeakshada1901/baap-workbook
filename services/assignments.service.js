const assignments = require("../models/assignments.model");
const assignments1 = {


  getAllassignments: async function () {
    return await assignments.find();
  },

  save: async function (assignmentsObject) {
    return await assignments.create(assignmentsObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await assignments.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },
//id
  
findByid: function(id) {
  return new Promise((resolve, reject) => {
    assignments.findById(id, function(err, doc) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        resolve(doc);
      }
    });
  });
},


// updateassignment: async function(id, topic) {
//   assignments.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
//     if(err){
//       console.log("error ", err)
//       return false;
//     } else {
//       console.log("dataa ",doc);
//       return doc
//     }
//   });
// },

// //id
// getassignmentsByUserId(user_id) {
//   return new Promise(async (resolve, reject) => {
//     try {
//       const assignments = await assignmentsModel.find({ "user_id": user_id }).exec();

//       resolve({
//         status: 200,
//         success: true,
//         data: assignments,
//         message: "fetched data sucessfully",
//       });

//     } catch (error) {
//       console.log(error);
//       reject({
//         status: 500,
//         success: false,
//         message: "Server Error",
//       });
//     }
//   });
// },
  // delete assignments
  deleteassignments: function(id) {
    return new Promise((resolve, reject) => {
      assignments.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      assignments.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  updateassignments: async function(id, topic) {
    assignments.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = assignments1;

