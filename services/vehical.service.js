const vehical = require("../models/vehicals.model");
const vehical1 = {


  getAllvehical: async function () {
    return await vehical.find();
  },

  save: async function (vehicalObject) {
    return await vehical.create(vehicalObject);
  },

//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     vehical.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },
  //find by name
  findByName: async function (name) {
    const data = await vehical.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete vehical
  deletevehical: function(id) {
    return new Promise((resolve, reject) => {
      vehical.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatevehical: async function(id, topic) {
    vehical.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = vehical1;

