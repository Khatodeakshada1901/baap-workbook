const { Promise } = require("mongoose");
const Collection = require("../models/app.model");

const app= {


  getAllCollection: async function () {
    return await Collection.find();
  },

  save: async function (collectionObject) {
    return await Collection.create(collectionObject);
  },

  //update collection.....
  updateCollection: function (id, topic) {
    Collection.findByIdAndUpdate(id, topic, { new: true }, function (err, doc) {
      if (err) {
        console.log("error ", err)
        return false;
      } else {
        console.log("data ", doc);
        return doc
      }
    });
  },

  //id
  //id
  getappByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const app = await appModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: app,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },
//id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     app.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },
  //Delete collection.......
  deleteCollection: function (id)
 {
    return new Promise((resolve, reject) => {
      Collection.findByIdAndDelete(id, function (err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },



};


module.exports = app;