const leave = require("../models/leave.model");
const leave1 = {


  getAllleave: async function () {
    return await leave.find();
  },

  save: async function (leaveObject) {
    return await leave.create(leaveObject);
  },


  //find by id
  findByid: function(id)
  {
     return new Promise((resolve, reject) => {
       leave.findById(id, function(err, doc) {
         if (err) {
           console.log(err);
           reject(err);
         } else {
           resolve(doc);
         }
       });
     });
   },
  //find by name
  findByName: async function (name) {
    const data = await leave.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete leave
  deleteleave: function(id) {
    return new Promise((resolve, reject) => {
      leave.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateleave: async function(id, topic) {
    leave.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = leave1;

