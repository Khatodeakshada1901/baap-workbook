const modul = require("../models/modul.model");
const modul1 = {


  getAllmodul: async function () {
    return await modul.find();
  },

  save: async function (modulObject) {
    return await modul.create(modulObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await modul.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getmodulByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const modul = await modulModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: modul,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletemodul: function(id) {
    return new Promise((resolve, reject) => {
      modul.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      modul.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatemodul: async function(id, topic) {
    modul.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = modul1;

