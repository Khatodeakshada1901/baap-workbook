const familyinfo = require("../models/familyinfo.model");
const familyinfo1 = {


  getAllfamilyinfo: async function () {
    return await familyinfo.find();
  },

  save: async function (familyinfoObject) {
    return await familyinfo.create(familyinfoObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await familyinfo.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getfamilyinfoByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const familyinfo = await familyinfoModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: familyinfo,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletefamilyinfo: function(id) {
    return new Promise((resolve, reject) => {
      familyinfo.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      familyinfo.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatefamilyinfo: async function(id, topic) {
    familyinfo.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = familyinfo1;

