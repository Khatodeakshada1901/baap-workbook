const subcategories = require("../models/subcategories.model");
const subcategories1 = {


  getAllsubcategories: async function () {
    return await subcategories.find();
  },

  save: async function (subcategoriesObject) {
    return await subcategories.create(subcategoriesObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     subcategories.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await subcategories.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete subcategories
  deletesubcategories: function(id) {
    return new Promise((resolve, reject) => {
      subcategories.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatesubcategories: async function(id, topic) {
    subcategories.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = subcategories1;

