const canteen = require("../models/canteen.model");
const canteen1 = {


  getAllcanteen: async function () {
    return await canteen.find();
  },

  save: async function (canteenObject) {
    return await canteen.create(canteenObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await canteen.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete canteen
  deletecanteen: function(id) {
    return new Promise((resolve, reject) => {
      canteen.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      canteen.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  updatecanteen: async function(id, topic) {
    canteen.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = canteen1;

