const addres = require("../models/addres.model");
const addres1 = {


  getAlladdres: async function () {
    return await addres.find();
  },

  save: async function (addresObject) {
    return await addres.create(addresObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await addres.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getaddresByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const addres = await addresModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: addres,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deleteaddres: function(id) {
    return new Promise((resolve, reject) => {
      addres.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      addres.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateaddres: async function(id, topic) {
    addres.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = addres1;

