const useraddres = require("../models/useraddres.model");
const useraddres1 = {


  getAlluseraddres: async function () {
    return await useraddres.find();
  },

  save: async function (useraddresObject) {
    return await useraddres.create(useraddresObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await useraddres.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },
  //find by id
  findByid: function(id)
  {
     return new Promise((resolve, reject) => {
       useraddres.findById(id, function(err, doc) {
         if (err) {
           console.log(err);
           reject(err);
         } else {
           resolve(doc);
         }
       });
     });
   },

  // delete useraddres
  deleteuseraddres: function(id) {
    return new Promise((resolve, reject) => {
      useraddres.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateuseraddres: async function(id, topic) {
    useraddres.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = useraddres1;

