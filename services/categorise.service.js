const categories = require("../models/categories.model");
const categories1 = {


  getAllcategories: async function () {
    return await categories.find();
  },

  save: async function (categoriesObject) {
    return await categories.create(categoriesObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await categories.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      categories.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  // delete categories
  deletecategories: function(id) {
    return new Promise((resolve, reject) => {
      categories.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatecategories: async function(id, topic) {
    categories.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = categories1;

