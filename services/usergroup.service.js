const usergroup = require("../models/usergroup.model");
const usergroup1 = {


  getAllusergroup: async function () {
    return await usergroup.find();
  },

  save: async function (usergroupObject) {
    return await usergroup.create(usergroupObject);
  },

//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     usergroup.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },
  //find by name
  findByName: async function (name) {
    const data = await usergroup.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete usergroup
  deleteusergroup: function(id) {
    return new Promise((resolve, reject) => {
      usergroup.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateusergroup: async function(id, topic) {
    usergroup.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = usergroup1;

