const family = require("../models/family.model");
const family1 = {


  getAllfamily: async function () {
    return await family.find();
  },

  save: async function (familyObject) {
    return await family.create(familyObject);
  },
//find by id
  findByid: function(id)
  {
     return new Promise((resolve, reject) => {
       family.findById(id, function(err, doc) {
         if (err) {
           console.log(err);
           reject(err);
         } else {
           resolve(doc);
         }
       });
     });
   },
   
  //find by name
  findByName: async function (name) {
    const data = await family.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete family
  deletefamily: function(id) {
    return new Promise((resolve, reject) => {
      family.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatefamily: async function(id, topic) {
    family.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = family1;

