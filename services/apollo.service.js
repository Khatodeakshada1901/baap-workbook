const apollo = require("../models/apollo.model");
const apollo1 = {


  getAllapollo: async function () {
    return await apollo.find();
  },

  save: async function (apolloObject) {
    return await apollo.create(apolloObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await apollo.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getapolloByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const apollo = await apolloModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: apollo,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deleteapollo: function(id) {
    return new Promise((resolve, reject) => {
      apollo.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      apollo.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateapollo: async function(id, topic) {
    apollo.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};
module.exports = apollo1;
