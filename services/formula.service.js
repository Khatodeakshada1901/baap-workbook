const formula = require("../models/formula.model");
const formula1 = {


  getAllformula: async function () {
    return await formula.find();
  },

  save: async function (formulaObject) {
    return await formula.create(formulaObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await formula.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getformulaByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const formula = await formulaModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: formula,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deleteformula: function(id) {
    return new Promise((resolve, reject) => {
      formula.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      formula.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateformula: async function(id, topic) {
    formula.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = formula1;