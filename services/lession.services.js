const lession  = require("../models/lession.model");
const lession1 = {


  getAlllession: async function () {
    return await lession.find();
  },

  save: async function (lessionObject) {
    return await lession.create(lessionObject);
  },

  //find by id
  findByid: function(id)
  {
     return new Promise((resolve, reject) => {
       lession.findById(id, function(err, doc) {
         if (err) {
           console.log(err);
           reject(err);
         } else {
           resolve(doc);
         }
       });
     });
   },

  //find by name
  findByName: async function (name) {
    const data = await lession.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete lession
  deletelession: function(id) {
    return new Promise((resolve, reject) => {
      lession.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatelession: async function(id, topic) {
    lession.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = lession1;

