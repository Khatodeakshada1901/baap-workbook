const shipping = require("../models/shipping.model");
const shipping1 = {


  getAllshipping: async function () {
    return await shipping.find();
  },

  save: async function (shippingObject) {
    return await shipping.create(shippingObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     shipping.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await shipping.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete shipping
  deleteshipping: function(id) {
    return new Promise((resolve, reject) => {
      shipping.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateshipping: async function(id, topic) {
    shipping.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = shipping1;

