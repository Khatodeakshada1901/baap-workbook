const developer = require("../models/developer.model");
const developer1 = {


  getAlldeveloper: async function () {
    return await developer.find();
  },

  save: async function (developerObject) {
    return await developer.create(developerObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await developer.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getdeveloperByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const developer = await developerModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: developer,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletedeveloper: function(id) {
    return new Promise((resolve, reject) => {
      developer.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      developer.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatedeveloper: async function(id, topic) {
    developer.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = developer1;

