const user = require("../models/user.model");
const user1 = {


  getAlluser: async function () {
    return await user.find();
  },

  save: async function (userObject) {
    return await user.create(userObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     user.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await user.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete user
  deleteuser: function(id) {
    return new Promise((resolve, reject) => {
      user.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateuser: async function(id, topic) {
    user.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = user1;

