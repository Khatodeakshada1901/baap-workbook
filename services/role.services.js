const role = require("../models/role.model");
const role1 = {


  getAllrole: async function () {
    return await role.find();
  },

  save: async function (roleObject) {
    return await role.create(roleObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     role.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await role.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete role
  deleterole: function(id) {
    return new Promise((resolve, reject) => {
      role.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updaterole: async function(id, topic) {
    role.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = role1;

