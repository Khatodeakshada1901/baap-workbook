const SuperAdmin = require("../models/SuperAdmin .model");
const SuperAdmin1 = {


  getAllSuperAdmin: async function () {
    return await SuperAdmin.find();
  },

  save: async function (SuperAdminObject) {
    return await SuperAdmin.create(SuperAdminObject);
  },

//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     SuperAdmin.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },
  //find by name
  findByName: async function (name) {
    const data = await SuperAdmin.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete SuperAdmin
  deleteSuperAdmin: function(id) {
    return new Promise((resolve, reject) => {
      SuperAdmin.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateSuperAdmin: async function(id, topic) {
    SuperAdmin.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = SuperAdmin1;

