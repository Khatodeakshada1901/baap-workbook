const permission = require("../models/permission.model");
const permission1 = {


  getAllpermission: async function () {
    return await permission.find();
  },

  save: async function (permissionObject) {
    return await permission.create(permissionObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     permission.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await permission.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete permission
  deletepermission: function(id) {
    return new Promise((resolve, reject) => {
      permission.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatepermission: async function(id, topic) {
    permission.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = permission1;

