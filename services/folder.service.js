const folder = require("../models/folder.model");
const folder1 = {


  getAllfolder: async function () {
    return await folder.find();
  },

  save: async function (folderObject) {
    return await folder.create(folderObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await folder.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getfolderByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const folder = await folderModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: folder,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletefolder: function(id) {
    return new Promise((resolve, reject) => {
      folder.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      folder.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatefolder: async function(id, topic) {
    folder.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = folder1;

