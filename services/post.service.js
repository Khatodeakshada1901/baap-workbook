const post = require("../models/post.model");
const post1 = {


  getAllpost: async function () {
    return await post.find();
  },

  save: async function (postObject) {
    return await post.create(postObject);
  },
//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     post.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },

  //find by name
  findByName: async function (name) {
    const data = await post.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete post
  deletepost: function(id) {
    return new Promise((resolve, reject) => {
      post.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatepost: async function(id, topic) {
    post.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = post1;

