const fee = require("../models/fee.model");
const fee1 = {


  getAllfee: async function () {
    return await fee.find();
  },

  save: async function (groupObject) {
    return await fee.create(groupObject);
  },

//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     fee.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },
 
  //find by name
  findByName: async function (name) {
    const data = await  fee.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // deletefee
  deletefee: function(id) {
    return new Promise((resolve, reject) => {
     fee.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatefee: async function(id, topic) {
   fee.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports =fee1;

