const window = require("../models/window.model");
const window1 = {


  getAllwindow: async function () {
    return await window.find();
  },

  save: async function (windowObject) {
    return await window.create(windowObject);
  },


  //find by name
  findByName: async function (name) {
    const data = await window.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },


  //id
  getwindowByUserId(user_id) {
    return new Promise(async (resolve, reject) => {
      try {
        const window = await windowModel.find({ "user_id": user_id }).exec();

        resolve({
          status: 200,
          success: true,
          data: window,
          message: "fetched data sucessfully",
        });

      } catch (error) {
        console.log(error);
        reject({
          status: 500,
          success: false,
          message: "Server Error",
        });
      }
    });
  },

  // delete Admi
  deletewindow: function(id) {
    return new Promise((resolve, reject) => {
      window.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },

  
  findByid: function(id) {
    return new Promise((resolve, reject) => {
      window.findById(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updatewindow: async function(id, topic) {
    window.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = window1;

