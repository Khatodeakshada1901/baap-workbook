const order = require("../models/order.model");
const order1 = {


  getAllorder: async function () {
    return await order.find();
  },

  save: async function (orderObject) {
    return await order.create(orderObject);
  },

//find by id
findByid: function(id)
{
   return new Promise((resolve, reject) => {
     order.findById(id, function(err, doc) {
       if (err) {
         console.log(err);
         reject(err);
       } else {
         resolve(doc);
       }
     });
   });
 },
  //find by name
  findByName: async function (name) {
    const data = await order.find({
      name: { $regex : '.*'+ name + '.*', $options:'i'} ,
    });
    return data;
  },

  // delete order
  deleteorder: function(id) {
    return new Promise((resolve, reject) => {
      order.findByIdAndDelete(id, function(err, doc) {
        if (err) {
          console.log(err);
          reject(err);
        } else {
          resolve(doc);
        }
      });
    });
  },
  
  
  updateorder: async function(id, topic) {
    order.findByIdAndUpdate(id, topic, {new: true}, function(err, doc){
      if(err){
        console.log("error ", err)
        return false;
      } else {
        console.log("dataa ",doc);
        return doc
      }
    });
  },
};



module.exports = order1;

